package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"sort"
	"strings"
	"time"
)

type slot struct {
	Label   string
	Lessons []lesson
}

type room struct {
	ID   int
	Name string
}

func newRoom(id int, name string) *room {
	return &room{id, name}
}

type lesson struct {
	room
	Trainer string
}

func newLesson(r room, trainer string) *lesson {
	return &lesson{r, trainer}
}

//Configuration is a complete set of a consecutive collection of slots, which contain a
//set of lessons which are then given to trainers
type Configuration struct {
	Slots []slot
}

//NewConfiguration sets up a new training configuration
func NewConfiguration(slotCount int, roomCount int) *Configuration {
	c := new(Configuration)
	if slotCount == 0 || roomCount == 0 {
		log.Fatalf("Invalid configuration: %v slots, %v rooms", slotCount, roomCount)
	}
	c.Slots = make([]slot, slotCount)
	for k := range c.Slots {
		c.Slots[k] = slot{Label: fmt.Sprintf("Day %d", k+1), Lessons: make([]lesson, roomCount)}
		for kk := range c.Slots[k].Lessons {
			c.Slots[k].Lessons[kk] = *newLesson(*newRoom(kk+1, fmt.Sprintf("Room %d", kk+1)), "")
		}
	}
	return c
}
func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func containsSet(s [][]string, e []string) bool {
	for _, a := range s {
		found := true
		for i, b := range a {
			if b == e[i] {
				found = false
			}
		}
		if found {
			return true
		}
	}
	return false
}

func remove(s []string, removeMe string) []string {
	for k, v := range s {
		if v == removeMe {
			s[len(s)-1], s[k] = s[k], s[len(s)-1]
			return s[:len(s)-1]
		}
	}
	return s
}

func (c *Configuration) placeTrainers(trainers []string) {
	usedCombinations := make([][]string, 0)
	availableTrainers := trainers
	for s := range c.Slots {
		combinationValid := false
		currentSet := make([]string, 0)
		for !combinationValid {
			currentSet = make([]string, 0)
			for l := range c.Slots[s].Lessons {
				if len(availableTrainers) == 0 {
					availableTrainers = trainers
				}
				t := availableTrainers[rand.Intn(len(availableTrainers))]
				for contains(currentSet, t) {
					t = availableTrainers[rand.Intn(len(availableTrainers))]
				}
				availableTrainers = remove(availableTrainers, t)
				c.Slots[s].Lessons[l].Trainer = t
				currentSet = append(currentSet, t)
			}
			sort.Strings(currentSet)
			//fmt.Println(currentSet)
			combinationValid = !containsSet(usedCombinations, currentSet)
		}
		usedCombinations = append(usedCombinations, currentSet)
		fmt.Println(usedCombinations)
	}
}

func main() {
	// seed rand...
	rand.Seed(time.Now().UnixNano())
	trainerInput := flag.String("trainers", "", "Names of trainers. No duplicates, comma-separated (required)")
	slotCount := flag.Int("lesson", 0, "Number of consecutive time slots (required)")
	roomCount := flag.Int("rooms", 0, "Number of rooms  (required)")
	flag.Parse()
	trainers := strings.Split(strings.TrimSpace(*trainerInput), ",")
	for k, v := range trainers {
		trainers[k] = strings.TrimSpace(v)
	}
	trainerCount := len(trainers)
	if trainerCount == 0 || *slotCount == 0 || *roomCount == 0 {
		flag.PrintDefaults()
		os.Exit(1)
	}
	log.Printf("Selected configuration: %v trainers to be placed in %v lessons in %v rooms in %v slots.\n", trainerCount, *roomCount**slotCount, *roomCount, *slotCount)

	c := NewConfiguration(*slotCount, *roomCount)

	c.placeTrainers(trainers)

	b, err := json.MarshalIndent(c, "", " ")
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(b))
}
